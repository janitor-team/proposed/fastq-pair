Source: fastq-pair
Section: science
Priority: optional
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Nilesh Patra <nilesh@debian.org>
Build-Depends: debhelper-compat (= 13), cmake
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/med-team/fastq-pair
Vcs-Git: https://salsa.debian.org/med-team/fastq-pair.git
Homepage: https://github.com/linsalrob/fastq-pair
Rules-Requires-Root: no

Package: fastq-pair
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Rewrites paired end fastq so all reads have a mate to separate out singletons
 This package rewrites the fastq files with the sequences in order,
 with matching files for the two files provided on the command line,
 and then any single reads that are not matched are place in two separate
 files, one for each original file.
 .
 This code is designed to be fast and memory efficient, and works with large
 fastq files. It does not store the whole file in memory, but rather just stores
 the locations of each of the indices in the first file provided in memory.
